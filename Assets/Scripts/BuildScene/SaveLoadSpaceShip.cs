﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameData;

public class SaveLoadSpaceShip : MonoBehaviour 
{
    static SaveLoadSpaceShip instance;
    public static SaveLoadSpaceShip Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    List<int> parts = new List<int>();
    List<Vector3> pos = new List<Vector3>();
    List<float> rot = new List<float>();

    public Transform ssRoot;
    public Transform previewRoot;

    //로컬에 저장 후 서버에도 올림.
    public void Save(int slotNum)
    {
        parts.Clear();
        pos.Clear();
        rot.Clear();

        foreach (Transform t in ssRoot.GetChild(0))//몸통
        {
            parts.Add(int.Parse(t.name.Substring(0, 2)));
            pos.Add(t.position);
            rot.Add(t.rotation.eulerAngles.z);
        }
        foreach (Transform t in ssRoot.GetChild(1))//주무장
        {
            parts.Add(int.Parse(t.name.Substring(0, 2)));
            pos.Add(t.position);
            rot.Add(t.rotation.eulerAngles.z);
        }
        foreach (Transform t in ssRoot.GetChild(2))//부무장
        {
            parts.Add(int.Parse(t.name.Substring(0, 2)));
            pos.Add(t.position);
            rot.Add(t.rotation.eulerAngles.z);
        }
        foreach (Transform t in ssRoot.GetChild(3))//추진
        {
            parts.Add(int.Parse(t.name.Substring(0, 2)));
            pos.Add(t.position);
            rot.Add(t.rotation.eulerAngles.z);
        }

        string onOffline = "";
        if (GameManager_ROOT.Instance.offlineMode)
        {
            onOffline = "off";
        }
        else
        {
            onOffline = "on";
        }

        //print("[세이브 시작] " + parts.Count + "개 부품");

        PlayerPrefs.SetInt(onOffline + slotNum + "ListCount", parts.Count);
        for(int i = 0; i<parts.Count; i++)
        {
            PlayerPrefs.SetInt(onOffline + slotNum + "_ItemCode" + i, parts[i]);
            PlayerPrefs.SetFloat(onOffline + slotNum + "_ItemPosX" + i, pos[i].x);
            PlayerPrefs.SetFloat(onOffline + slotNum + "_ItemPosY" + i, pos[i].y);
            PlayerPrefs.SetFloat(onOffline + slotNum + "_ItemRotZ" + i, rot[i]);
            //print(parts[i] + " / " + pos[i] + " / " + rot[i]);
        }
        PlayerPrefs.Save();

        //print("[세이브 끝]");

        if (!GameManager_ROOT.Instance.offlineMode)
        {
            UploadToServer();
        }
    }

    public GameObject[] partsPF = new GameObject[10];

    //로컬에서 불러오는거임.
    public void Load(int slotNum)
    {
        string onOffline = "";
        if (GameManager_ROOT.Instance.offlineMode)
        {
            onOffline = "off";
        }
        else
        {
            onOffline = "on";
        }

        int listCount = PlayerPrefs.GetInt(onOffline + slotNum + "ListCount");

        //print("[로드 시작] " + listCount + "개 부품");

        parts.Clear();
        pos.Clear();
        rot.Clear();

        for (int i = 0; i < listCount; i++)
        {
            parts.Add(PlayerPrefs.GetInt(onOffline + slotNum + "_ItemCode" + i));
            pos.Add(new Vector3(PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosX" + i), PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosY" + i),0));
            rot.Add(PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemRotZ" + i));
            //print(parts[i] + " / " + pos[i] + " / " + rot[i]);
        }

        //print("[로드 끝]");

        foreach (Transform t in ssRoot.transform.GetChild(0))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in ssRoot.transform.GetChild(1))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in ssRoot.transform.GetChild(2))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in ssRoot.transform.GetChild(3))
        {
            Destroy(t.gameObject);
        }

        for(int i = 0; i<listCount; i++)
        {
            GameObject go = Instantiate(partsPF[parts[i]], pos[i], Quaternion.Euler(0,0,rot[i])) as GameObject;
            if (go.name == "00_TankCTR(Clone)")
            {
                GameManager_build.Instance.startBlock = go;
            }
            else
            {
                go.GetComponent<MouseDrag>().newObj = false;
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.Body)
            {
                go.transform.SetParent(ssRoot.transform.GetChild(0));
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.MainWeapon)
            {
                go.transform.SetParent(ssRoot.transform.GetChild(1));
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.Weapon)
            {
                go.transform.SetParent(ssRoot.transform.GetChild(2));
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.Thruster)
            {
                go.transform.SetParent(ssRoot.transform.GetChild(3));
            }
        }

    }

    public void LoadPreview(int slotNum)
    {
        string onOffline = "";
        if (GameManager_ROOT.Instance.offlineMode)
        {
            onOffline = "off";
        }
        else
        {
            onOffline = "on";
        }

        int listCount = PlayerPrefs.GetInt(onOffline + slotNum + "ListCount");

        //print("[로드 시작] " + listCount + "개 부품");

        parts.Clear();
        pos.Clear();
        rot.Clear();

        for (int i = 0; i < listCount; i++)
        {
            parts.Add(PlayerPrefs.GetInt(onOffline + slotNum + "_ItemCode" + i));
            pos.Add(new Vector3(PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosX" + i), PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosY" + i), 0));
            rot.Add(PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemRotZ" + i));
            //print(parts[i] + " / " + pos[i] + " / " + rot[i]);
        }

        //print("[로드 끝]");

        foreach (Transform t in previewRoot.transform.GetChild(0))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in previewRoot.transform.GetChild(1))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in previewRoot.transform.GetChild(2))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in previewRoot.transform.GetChild(3))
        {
            Destroy(t.gameObject);
        }

        for (int i = 0; i < listCount; i++)
        {
            GameObject go = Instantiate(partsPF[parts[i]], pos[i], Quaternion.Euler(0, 0, rot[i])) as GameObject;
            go.tag = "Untagged";
            go.layer = 2;
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.Body)
            {
                go.transform.SetParent(previewRoot.transform.GetChild(0),false);
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.MainWeapon)
            {
                go.transform.SetParent(previewRoot.transform.GetChild(1), false);
                go.GetComponent<Fire>().enabled = false;
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.Weapon)
            {
                go.transform.SetParent(previewRoot.transform.GetChild(2), false);
                go.GetComponent<Fire>().enabled = false;
            }
            if (go.GetComponent<PartsInfo>().type == PartsType.Type.Thruster)
            {
                go.transform.SetParent(previewRoot.transform.GetChild(3), false);
            }
        }

    }
    //----------------------------------.

    string[] data = new string[3];
    public void UploadToServer()
    {
        string onOffline = "";
        if (GameManager_ROOT.Instance.offlineMode)
        {
            onOffline = "off";
        }
        else
        {
            onOffline = "on";
        }

        for (int slotNum = 1; slotNum <= 3; slotNum++)
        {
            int listCount = PlayerPrefs.GetInt(onOffline + slotNum + "ListCount");

            string s_code = "";
            string s_posX = "";
            string s_posY = "";
            string s_rotZ = "";

            for (int i = 0; i < listCount; i++)
            {
                if (i == listCount - 1)
                {
                    s_code = s_code + PlayerPrefs.GetInt(onOffline + slotNum + "_ItemCode" + i) + "/";
                }
                else
                {
                    s_code = s_code + PlayerPrefs.GetInt(onOffline + slotNum + "_ItemCode" + i) + ",";
                }
            }
            for (int i = 0; i < listCount; i++)
            {
                if (i == listCount - 1)
                {
                    s_posX = s_posX + PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosX" + i) + "/";
                }
                else
                {
                    s_posX = s_posX + PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosX" + i) + ",";
                }
            }
            for (int i = 0; i < listCount; i++)
            {
                if (i == listCount - 1)
                {
                    s_posY = s_posY + PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosY" + i) + "/";
                }
                else
                {
                    s_posY = s_posY + PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemPosY" + i) + ",";
                }
            }
            for (int i = 0; i < listCount; i++)
            {
                if (i == listCount - 1)
                {
                    s_rotZ = s_rotZ + PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemRotZ" + i);
                }
                else
                {
                    s_rotZ = s_rotZ + PlayerPrefs.GetFloat(onOffline + slotNum + "_ItemRotZ" + i) + ",";
                }
            }

            data[slotNum-1] = listCount + "/" + s_code + s_posX + s_posY + s_rotZ;
        }

        StartCoroutine(SaveShipCor());
    }

    IEnumerator SaveShipCor()
    {
        string url = "http://52.78.137.62/SpaceShip/SaveShip.php";
        WWWForm form = new WWWForm();
        form.AddField("userID", PlayerPrefs.GetString("userID"));
        form.AddField("slotNum1", data[0]);
        form.AddField("slotNum2", data[1]);
        form.AddField("slotNum3", data[2]);

        WWW www = new WWW(url, form);
        yield return www;

        if (www.error == null)
        {
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text.Trim()) as Dictionary<string, object>;
            string code = responseData["code"].ToString().Trim();

            if (code == "UPDATE_FAIL" || code == "INSERT_FAIL")
            {
                print(code);
                yield break;
            }

            print(code);
        }
        else
        {
            print("서버연결실패");
        }
    }
}
