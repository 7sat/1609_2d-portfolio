﻿using UnityEngine;
using System.Collections;

public class MouseDrag : MonoBehaviour
{
    bool Drag = false;
    bool canPlace = false;

    public bool newObj = true;
    Vector2 oldPos;

    public void DragStart()
    {
        oldPos = transform.position;
        UIManager.Instance.selected = gameObject;

        foreach (Transform c in transform)
        {
            c.gameObject.layer = 2;
        }

        GameObject[] cube = GameObject.FindGameObjectsWithTag("Cube");
        if(GetComponent<PartsInfo>().type == PartsType.Type.Body || GetComponent<PartsInfo>().type == PartsType.Type.Thruster)
        {
            foreach (GameObject c in cube)
            {
                GameObject snapPosRoot = c.transform.GetChild(0).gameObject;
                snapPosRoot.SetActive(true);

                int childIndex = 0;

                foreach (Transform p in snapPosRoot.transform)
                {
                    bool overlap = false;

                    Collider2D[] col = Physics2D.OverlapPointAll(p.transform.position);
                    if (col != null)
                    {
                        foreach(Collider2D cc in col)
                        {
                            if (cc.gameObject.tag == "Cube" || cc.gameObject.tag == "Out")
                            {
                                overlap = true;
                            }
                        }
                    }

                    if(overlap)
                    {
                        p.gameObject.SetActive(false);
                    }
                    else
                    {
                        if(GetComponent<PartsInfo>().type == PartsType.Type.Thruster)
                        {
                            if(childIndex == 3)
                            {
                                p.gameObject.SetActive(true);
                            }
                            else
                            {
                                p.gameObject.SetActive(false);
                            }
                        }
                        else
                        {
                            p.gameObject.SetActive(true);
                        }
                    }
                    childIndex += 1;
                }
            }
            transform.GetChild(0).gameObject.SetActive(false);
        }
        else
        {
            foreach (GameObject c in cube)
            {
                GameObject snapPosRoot = c.transform.GetChild(1).gameObject;
                snapPosRoot.SetActive(true);

                foreach (Transform p in snapPosRoot.transform)
                {
                    bool overlap = false;

                    Collider2D[] col = Physics2D.OverlapPointAll(p.transform.position);
                    if (col != null)
                    {
                        foreach (Collider2D cc in col)
                        {
                            if (cc.gameObject.tag == "Cube" && cc.gameObject.GetComponent<PartsInfo>().type != PartsType.Type.Body)
                            {
                                overlap = true;
                            }
                        }
                    }

                    if (overlap)
                    {
                        p.gameObject.SetActive(false);
                    }
                    else
                    {
                        p.gameObject.SetActive(true);
                    }
                }
            }
            transform.GetChild(1).gameObject.SetActive(false);
        }

        Drag = true;
    }

    public void DragFinish()
    {
        UIManager.Instance.selected = null;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        Drag = false;

        if(!canPlace) //스냅포인트에 위치하지 않음
        {
            GameManager_ROOT.Instance.playInstallCancel();
            UIManager.Instance.BuildMode = false;
            if(newObj)
            {
                Destroy(gameObject);
            }
            else
            {
                transform.position = oldPos;
            }
        }
        else
        {
            GameManager_ROOT.Instance.playInstall();
            newObj = false;
        }

        foreach (Transform c in transform)
        {
            c.gameObject.layer = 8;
        }

        GameObject[] cube = GameObject.FindGameObjectsWithTag("Cube");
        foreach (GameObject c in cube)
        {
            c.transform.GetChild(0).gameObject.SetActive(false);
            foreach (Transform t in c.transform.GetChild(0))
            {
                t.gameObject.SetActive(true);
            }
            c.transform.GetChild(1).gameObject.SetActive(false);
        }
    }

    void Update()
    {
        if (!Drag) return;

        Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 objPos = Camera.main.ScreenToWorldPoint(mousePos);

        int layermask = 1 << 8;

        Collider2D hit = Physics2D.OverlapPoint(objPos, layermask);

        if (hit != null)
        {
            canPlace = true;
            transform.position = hit.transform.position;
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
        else
        {
            canPlace = false;
            transform.position = objPos;
            GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
        }
    }
}