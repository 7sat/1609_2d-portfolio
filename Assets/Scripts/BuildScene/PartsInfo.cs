﻿using UnityEngine;
using System.Collections;

public class PartsType
{
    public enum Type { Body, MainWeapon, Weapon, Thruster };
}

public class PartsInfo : MonoBehaviour 
{
    public int ITEMCODE;
    public int weight;
    public int cost;

    public PartsType.Type type;
}
