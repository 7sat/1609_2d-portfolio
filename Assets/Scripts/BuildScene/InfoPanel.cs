﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour 
{
    static InfoPanel instance;
    public static InfoPanel Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    public GameObject lowhp;
    public GameObject lowdmg;
    public GameObject lowspd;
    public GameObject cantmove;
    public GameObject cantattack;
    public GameObject connecterr;

    public GameObject[] cubes;
    public int bodyCount;
    public int weaponCount;
    public int thrustCount;

    public void Start()
    {
        InvokeRepeating("Check", 1f, 1f);
    }

    public void Check()
    {
        cubes = GameObject.FindGameObjectsWithTag("Cube");
        bodyCount = 0;
        weaponCount = 0;
        thrustCount = 0;
        lowhpCheck();
        weaponCheck();
        cantmoveCheck();
    }

    public void lowhpCheck()
    {
        foreach(GameObject t in cubes)
        {
            if (t.GetComponent<PartsInfo>().type == PartsType.Type.Body)
            {
                bodyCount += 1;
            }
        }
        if(bodyCount < 6)
        {
            lowhp.SetActive(true);
        }
        else
        {
            lowhp.SetActive(false);
        }
    }
    public void weaponCheck()
    {
        foreach (GameObject t in cubes)
        {
            if (t.GetComponent<PartsInfo>().type == PartsType.Type.Weapon || t.GetComponent<PartsInfo>().type == PartsType.Type.MainWeapon)
            {
                weaponCount += 1;
            }
        }
        if (weaponCount == 0)
        {
            cantattack.SetActive(true);
            lowdmg.SetActive(false);
        }
        else if (weaponCount < 3)
        {
            cantattack.SetActive(false);
            lowdmg.SetActive(true);
        }
        else
        {
            cantattack.SetActive(false);
            lowdmg.SetActive(false);
        }
    }
    public void cantmoveCheck()
    {
        foreach (GameObject t in cubes)
        {
            if (t.GetComponent<PartsInfo>().type == PartsType.Type.Thruster)
            {
                thrustCount += 1;
            }
        }
        if (thrustCount == 0)
        {
            cantmove.SetActive(true);
            lowspd.SetActive(false);
        }
        else if (thrustCount == 1)
        {
            cantmove.SetActive(false);
            lowspd.SetActive(true);
        }
        else
        {
            cantmove.SetActive(false);
            lowspd.SetActive(false);
        }
    }
}
