﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager_build : MonoBehaviour 
{
    static GameManager_build instance;
    public static GameManager_build Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        //waitPanel.SetActive(true);
        //CreatePreviewImg();
    }

    public GameObject startBlock;
    public int uncheckedBlockCount;

    public GameObject buildFinishBtn;

    public GameObject waitPanel;

    public void ConnectCheckStart()
    {
        buildFinishBtn.GetComponent<Button>().interactable = false;
        InfoPanel.Instance.connecterr.SetActive(true);

        GameObject[] blocks = GameObject.FindGameObjectsWithTag("Cube");
        //print(blocks.Length + "개 블록 있음");
        uncheckedBlockCount = 0;

        foreach (GameObject go in blocks)
        {
            if (go == UIManager.Instance.selected) break;

            if(go.GetComponent<PartsInfo>().type == PartsType.Type.Body)
            {
                uncheckedBlockCount += 1;
            }
            go.GetComponent<SpriteRenderer>().color = Color.red;
            go.GetComponent<ConnectCheck>().isChecked = false;
        }

        startBlock.GetComponent<SpriteRenderer>().color = Color.white;
        startBlock.GetComponent<ConnectCheck>().isChecked = true;
        startBlock.GetComponent<ConnectCheck>().CheckConnect();

        StopAllCoroutines();
        StartCoroutine(waitUntilCheckFinish());
    }

    IEnumerator waitUntilCheckFinish()
    {
        while(uncheckedBlockCount>0)
        {
            yield return new WaitForSeconds(1f);
        }
        buildFinishBtn.GetComponent<Button>().interactable = true;
        InfoPanel.Instance.connecterr.SetActive(false);
    }

    public void CreatePreviewImg()
    {
        StartCoroutine(CreatePrevieImgCor());
    }

    IEnumerator CreatePrevieImgCor()
    {
        for (int i = 3; i > 0; i--)
        {
            SaveLoadSpaceShip.Instance.Load(i);

            yield return new WaitForSeconds(0.5f);

            CaptureRenderTexture.Instance.Capture(i.ToString());
        }
        waitPanel.SetActive(false);
    }
}
