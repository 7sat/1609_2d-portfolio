﻿using UnityEngine;
using System.Collections;

public class ConnectCheck : MonoBehaviour 
{
    public bool isChecked = false;

    public void CheckConnect()
    {
        isChecked = true;
        GameManager_build.Instance.uncheckedBlockCount -= 1;

        foreach(Transform t in transform.GetChild(0))
        {
            GameObject selectedOBJ = null;

            Collider2D[] col = Physics2D.OverlapPointAll(t.position);

            foreach(Collider2D c in col)
            {
                if (c.tag == "Out" || c.name == "Tank(root)") break;

                selectedOBJ = c.gameObject;

                selectedOBJ.GetComponent<SpriteRenderer>().color = Color.white;
                
                if (selectedOBJ.GetComponent<PartsInfo>().type == PartsType.Type.Thruster)
                {
                    Collider2D col2 = Physics2D.OverlapPoint(selectedOBJ.transform.GetChild(2).position);
                    if (col2 == null)
                    {
                        selectedOBJ.GetComponent<SpriteRenderer>().color = Color.red;
                        isChecked = false;
                        GameManager_build.Instance.uncheckedBlockCount += 1;
                    }
                }
                else if (selectedOBJ.GetComponent<PartsInfo>().type == PartsType.Type.Body)
                {
                    if (selectedOBJ.GetComponent<ConnectCheck>().isChecked) break;

                    selectedOBJ.GetComponent<ConnectCheck>().CheckConnect();
                }
            }
        }
    }
}
