﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour 
{
    static UIManager instance;
    public static UIManager Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    public GameObject tank_Root;

    public GameObject body1PF;
    public GameObject body1cPF;
    public GameObject body2PF;
    public GameObject mainWeapon1PF;
    public GameObject subWeapon1PF;
    public GameObject subWeapon2PF;
    public GameObject thruster1PF;

    public GameObject selected;
    int angle;

    public bool BuildMode = false;
    GameObject build_PF;
    int buil_childNum;
    //메뉴 페널 보이기 숨기기------------------------.
    public GameObject rootPanel;
    public GameObject bodyPanel;
    public GameObject movePanel;
    public GameObject mainWeaponPanel;
    public GameObject subWeaponPanel;
    public GameObject saveLoadPanel;
    public GameObject previewPanel;

    public Image previewImg;

    public void ShowRootPanel()
    {
        GameManager_ROOT.Instance.playPage();
        bodyPanel.SetActive(false);
        movePanel.SetActive(false);
        mainWeaponPanel.SetActive(false);
        subWeaponPanel.SetActive(false);
        saveLoadPanel.SetActive(false);
        previewPanel.SetActive(false);

        rootPanel.GetComponent<Animator>().Play("CategoryPanelOpen");
    }
    public void ShowBodyPanel()
    {
        GameManager_ROOT.Instance.playPage();
        bodyPanel.SetActive(true);

        rootPanel.GetComponent<Animator>().Play("CategoryPanelClose");
    }
    public void ShowMovePanel()
    {
        GameManager_ROOT.Instance.playPage();
        movePanel.SetActive(true);

        rootPanel.GetComponent<Animator>().Play("CategoryPanelClose");
    }
    public void ShowMainWeaponPanel()
    {
        GameManager_ROOT.Instance.playPage();
        mainWeaponPanel.SetActive(true);

        rootPanel.GetComponent<Animator>().Play("CategoryPanelClose");
    }
    public void ShowSubWeaponPanel()
    {
        GameManager_ROOT.Instance.playPage();
        subWeaponPanel.SetActive(true);

        rootPanel.GetComponent<Animator>().Play("CategoryPanelClose");
    }
    public void ShowSaveLoadPanel()
    {
        GameManager_ROOT.Instance.playPage();
        saveLoadPanel.SetActive(true);

        rootPanel.GetComponent<Animator>().Play("CategoryPanelClose");
    }

    int seletedSlotIndex = 1;
    public GameObject[] slotBtn = new GameObject[3];
    public void slotBtnClick(int slotNum)
    {
        GameManager_ROOT.Instance.playClick();
        slotBtn[0].GetComponent<Image>().color = Color.white;
        slotBtn[1].GetComponent<Image>().color = Color.white;
        slotBtn[2].GetComponent<Image>().color = Color.white;
        slotBtn[slotNum-1].GetComponent<Image>().color = Color.green;
        seletedSlotIndex = slotNum;
        previewPanel.SetActive(true);
        SaveLoadSpaceShip.Instance.LoadPreview(slotNum);
        //string name = slotNum.ToString();
        //AssetDatabase.Refresh();
        //previewImg.sprite = Resources.Load<Sprite>(name);
    }
    public void SaveBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        confirmPopup.Instance.ShowPopup("저장 확인", "기존의 파일을 덮어씁니다.");
        StartCoroutine(save_waitForResponce());
    }
    public void LoadBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        confirmPopup.Instance.ShowPopup("불러오기 확인", "저장하지 않은 정보는 잃게 됩니다.");
        StartCoroutine(load_waitForResponce());
    }
    IEnumerator save_waitForResponce()
    {
        while(!GameManager_ROOT.Instance.confirm && !GameManager_ROOT.Instance.cancel)
        {
            yield return null;
        }
        if(GameManager_ROOT.Instance.confirm)
        {
            //CaptureRenderTexture.Instance.Capture(seletedSlotIndex.ToString());
            previewPanel.SetActive(false);
            SaveLoadSpaceShip.Instance.Save(seletedSlotIndex);
        }
    }
    IEnumerator load_waitForResponce()
    {
        while (!GameManager_ROOT.Instance.confirm && !GameManager_ROOT.Instance.cancel)
        {
            yield return null;
        }
        if (GameManager_ROOT.Instance.confirm)
        {
            SaveLoadSpaceShip.Instance.Load(seletedSlotIndex);
            previewPanel.SetActive(false);
        }
    }
    //부품 생성------------------------.
    public void NewBody1()
    {
        createObj(body1PF,0);
    }
    public void NewBody1c()
    {
        createObj(body1cPF, 0);
    }
    public void NewBody2()
    {
        createObj(body2PF, 0);
    }
    public void NewMainWeapon1()
    {
        createObj(mainWeapon1PF,1);
    }
    public void NewSubWeapon1()
    {
        createObj(subWeapon1PF, 2);
    }
    public void NewSubWeapon2()
    {
        createObj(subWeapon2PF, 2);
    }
    public void NewThruster1()
    {
        createObj(thruster1PF, 3);
    }

    void createObj(GameObject objPF,int childNum)
    {
        if (!BuildMode)
        {
            angle = 0;
            GameManager_ROOT.Instance.playClick();
        }

        build_PF = objPF;
        buil_childNum = childNum;

        Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector2 objPos = Camera.main.ScreenToWorldPoint(mousePos);

        GameObject go = Instantiate(objPF, objPos, Quaternion.Euler(0,0,angle)) as GameObject;
        go.GetComponent<MouseDrag>().DragStart();
        go.transform.SetParent(tank_Root.transform.GetChild(childNum));
        selected = go;
        go.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);

        BuildMode = true;
    }
    //--------------------------------.

    public bool screenBlocked = false;

    public Text money;
    void Start()
    {
        if(GameManager_ROOT.Instance.offlineMode)
        {
            GameManager_ROOT.Instance.money = PlayerPrefs.GetInt("offmoney");
            money.text = GameManager_ROOT.Instance.money.ToString();
            print("오프라인 돈 불러옴");
        }
        else
        {
            GameManager_ROOT.Instance.money = PlayerPrefs.GetInt("onmoney");
            money.text = GameManager_ROOT.Instance.money.ToString();
            print("온라인 돈 불러옴");
        }
    }

    void Update()
    {
        if (screenBlocked) return;

        //버리기.
        if (selected != null && Input.GetMouseButtonDown(0))
        {
            Collider2D col = Physics2D.OverlapPoint(Input.mousePosition);
            if (col != null)
            {
                if(col.tag == "DelBtn")
                {
                    Destroy(selected);
                    Invoke("check", 0.05f);
                }
            }
        }

        //기존에 생성한 블록을 클릭하거나, 드래그 중인걸 내려놓기.
        if (Input.GetMouseButtonDown(0))
        {
            if (selected == null) // 기존에 선택된게 없음
            {
                Collider2D[] col = Physics2D.OverlapPointAll(Camera.main.ScreenToWorldPoint(Input.mousePosition));

                if (col.Length == 0 || col[0].tag == "Out") return;

                GameObject selectedOBJ = null;

                foreach (Collider2D c in col)
                {
                    if (c.gameObject.layer == LayerMask.NameToLayer("snapPoint") || c.name == "00_TankCTR" || c.name == "00_TankCTR(Clone)")
                    {
                        break;
                    }
                    selectedOBJ = c.gameObject;
                    if (c.GetComponent<PartsInfo>().type != PartsType.Type.Body)
                    {
                        break;
                    }
                }

                if (selectedOBJ != null)
                {
                    selectedOBJ.GetComponent<MouseDrag>().DragStart();
                }
            }
            else // 선택해서 드래그 중이던것을 내려놓음. // 만약 빌드모드이면 1)내려놓고 바로 2)새 블록 생성 및 3)드래그 시작.
            {
                selected.GetComponent<MouseDrag>().DragFinish();

                if(BuildMode)
                {
                    GameManager_build.Instance.ConnectCheckStart();
                    createObj(build_PF, buil_childNum);
                }
                else
                {
                    Invoke("check", 0.05f);
                }
            }
        }

        if(Input.GetMouseButtonDown(1))
        {
            BuildMode = false;

            if(selected != null)
            {
                GameObject[] cube = GameObject.FindGameObjectsWithTag("Cube");
                foreach (GameObject c in cube)
                {
                    c.transform.GetChild(0).gameObject.SetActive(false);
                    c.transform.GetChild(1).gameObject.SetActive(false);
                }

                Destroy(selected);
                Invoke("check", 0.05f);
            }
        }

        if(Input.GetMouseButtonDown(2))
        {
            if(selected != null)
            {
                if(selected.GetComponent<PartsInfo>().type == PartsType.Type.MainWeapon || selected.GetComponent<PartsInfo>().type == PartsType.Type.Weapon || selected.name == "02_TankBody_1c(Clone)")
                {
                    angle += 90;
                    selected.transform.Rotate(0, 0, 90);
                }
            }
        }
    }

    void check()
    {
        GameManager_build.Instance.ConnectCheckStart();
    }

    public void delAllBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        confirmPopup.Instance.ShowPopup("전체 버리기 확인", "설치한 모든 블럭을 버립니다.");
        StartCoroutine(delAll_waitForResponce());
    }
    IEnumerator delAll_waitForResponce()
    {
        while (!GameManager_ROOT.Instance.confirm && !GameManager_ROOT.Instance.cancel)
        {
            yield return null;
        }
        if (GameManager_ROOT.Instance.confirm)
        {
            DestroyAll();
        }
    }
    public void DestroyAll()
    {
        foreach (Transform t in tank_Root.transform.GetChild(0))
        {
            if(t.name == "00_TankCTR" || t.name == "00_TankCTR(Clone)")
            {

            }
            else
            {
                Destroy(t.gameObject);
            }
        }
        foreach (Transform t in tank_Root.transform.GetChild(1))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in tank_Root.transform.GetChild(2))
        {
            Destroy(t.gameObject);
        }
        foreach (Transform t in tank_Root.transform.GetChild(3))
        {
            Destroy(t.gameObject);
        }
    }

    public void LogOutBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        confirmPopup.Instance.ShowPopup("로그아웃 확인", "저장하지 않은 정보는 잃게 됩니다.");
        StartCoroutine(logOut_waitForResponce());
    }
    IEnumerator logOut_waitForResponce()
    {
        while (!GameManager_ROOT.Instance.confirm && !GameManager_ROOT.Instance.cancel)
        {
            yield return null;
        }
        if (GameManager_ROOT.Instance.confirm)
        {
            LogOut();
        }
    }
    public void LogOut()
    {
        Destroy(tank_Root);
        SceneManager.LoadScene("Login");
    }

    public void GameStart()
    {
        GameManager_ROOT.Instance.playClick();
        Destroy(selected);
        GameManager_ROOT.Instance.playing = true;
        GameManager_ROOT.Instance.gameover = false;
        InfoPanel.Instance.Check();
        GameObject.Find("Tank(root)").GetComponent<PlayerShipCtr>().enabled = true;
        GameManager_ROOT.Instance.playGameBgm();
        SceneManager.LoadScene("Game");
    }
}
