﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class confirmPopup : MonoBehaviour 
{
    static confirmPopup instance;
    public static confirmPopup Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    public GameObject confirmPanel;
    public Text titleText;
    public Text textText;
    public GameObject confirmBtn;

	public void ShowPopup(string title, string text)
    {
        GameManager_ROOT.Instance.playErr();
        GameManager_ROOT.Instance.confirm = false;
        GameManager_ROOT.Instance.cancel = false;
        UIManager.Instance.screenBlocked = true;

        titleText.text = title;
        textText.text = text;
        confirmPanel.SetActive(true);
    }

    public void ConfirmBtnClick()
    {
        GameManager_ROOT.Instance.playSave();
        GameManager_ROOT.Instance.confirm = true;
        UIManager.Instance.screenBlocked = false;
        confirmPanel.SetActive(false);
    }

    public void CancelBtnClick()
    {
        GameManager_ROOT.Instance.playCancel();
        GameManager_ROOT.Instance.cancel = true;
        UIManager.Instance.screenBlocked = false;
        confirmPanel.SetActive(false);
    }
}
