﻿using UnityEngine;
using System.Collections;
using System.IO;

public class CaptureRenderTexture : MonoBehaviour
{
    static CaptureRenderTexture instance;
    public static CaptureRenderTexture Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    public Camera cam;

    public void Capture(string fileName)
    {
        cam.Render();
        RenderTexture rt = gameObject.GetComponent<Renderer>().material.mainTexture as RenderTexture;
        Texture2D captureTexture = Texture2DGetRenderTexture(rt);

        byte[] data = captureTexture.EncodeToPNG();

        //FileStream fs = new FileStream(Application.dataPath + "/Resources/" + fileName + ".png", FileMode.OpenOrCreate);
        //fs.Write(data, 0, data.Length);
        //fs.Close();

        System.IO.File.WriteAllBytes(Application.dataPath + "/Resources/" + fileName + ".png", captureTexture.EncodeToPNG());

        Texture2D.DestroyImmediate(captureTexture, true);
    }

    public Texture2D Texture2DGetRenderTexture(RenderTexture rt)
    {
        RenderTexture.active = rt;
        Texture2D tempTexture = new Texture2D(rt.width, rt.height);
        tempTexture.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        tempTexture.Apply();

        return tempTexture;
    }
}
