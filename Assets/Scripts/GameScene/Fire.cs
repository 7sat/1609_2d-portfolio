﻿using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour 
{
    public GameObject bulletPF;
    public Transform shotPos;

    public float fireRate;
    float lastFire;

    public bool canRotate;
    public float rotateLimit;
    public float rotateSpeed;

    public float baseRot;

    public bool missile;
    public bool missileReady = true;

    SpriteRenderer sr;
    public Sprite missileBase;
    public Sprite missileBaseEmpty;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        baseRot = transform.eulerAngles.z;
    }

    void Update()
    {
        if (transform.root.GetComponent<SpaceShipInfo>().enemy || !canRotate || !GameManager_ROOT.Instance.playing) return;

        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        float angle = rot_z - 90;
        Quaternion qa = Quaternion.Euler(0f, 0f, angle);

        float bodyA = transform.root.transform.eulerAngles.z;
        float partA = qa.eulerAngles.z;
        float degree = Mathf.Abs(Mathf.DeltaAngle(bodyA, partA));
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, qa, Time.deltaTime * rotateSpeed);

        if (degree < rotateLimit)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, qa, Time.deltaTime * rotateSpeed);
        }
    }

    public void shot()
    {
        if (missile)
        {
            if (!missileReady) return;

            StartCoroutine(shotMissile());
            GameObject go = (GameObject)Instantiate(bulletPF, shotPos.position, shotPos.rotation);
            go.GetComponent<bulletMove>().enemy = transform.root.GetComponent<SpaceShipInfo>().enemy;
            if (go.GetComponent<bulletMove>().enemy)
            {
                go.GetComponent<Rigidbody2D>().velocity = shotPos.up * 7;
            }
            else
            {
                go.GetComponent<Rigidbody2D>().velocity = shotPos.up * 14;
            }
            go.GetComponent<Rigidbody2D>().AddForce(transform.root.GetComponent<Rigidbody2D>().velocity * 50);
        }
        else
        {
            if(Time.time-lastFire > fireRate)
            {
                lastFire = Time.time;
                GameObject go = (GameObject)Instantiate(bulletPF, shotPos.position, shotPos.rotation);
                go.GetComponent<bulletMove>().enemy = transform.root.GetComponent<SpaceShipInfo>().enemy;
                if (go.GetComponent<bulletMove>().enemy)
                {
                    go.GetComponent<Rigidbody2D>().velocity = shotPos.up * 7;
                }
                else
                {
                    go.GetComponent<Rigidbody2D>().velocity = shotPos.up * 14;
                }
                go.GetComponent<Rigidbody2D>().AddForce(transform.root.GetComponent<Rigidbody2D>().velocity * 50);
            }
        }
    } 

    IEnumerator shotMissile()
    {
        missileReady = false;
        sr.sprite = missileBaseEmpty;
        yield return new WaitForSeconds(fireRate);
        sr.sprite = missileBase;
        missileReady = true;
    }
}
