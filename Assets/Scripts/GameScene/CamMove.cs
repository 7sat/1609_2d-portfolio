﻿using UnityEngine;
using System.Collections;

public class CamMove : MonoBehaviour 
{
    public bool CamFollowPlayer = false;
    float camZoffset = -10;

	void Start () 
	{
        StartCoroutine(zzoomOut());
	}

    IEnumerator zzoomOut()
    {
        float size = 3;
        while(size<18)
        {
            size += 0.5f;
            Camera.main.orthographicSize = size;
            yield return null;
        }
        CamFollowPlayer = true;
    }

    void FixedUpdate()
    {
        if (!CamFollowPlayer || GameManager_ROOT.Instance.gameover) return;

        if(Input.GetKey(KeyCode.LeftShift))
        {
            Vector3 targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - GameManager.Instance.playerObj.transform.position;
            transform.position = Vector3.Lerp(transform.position, GameManager.Instance.playerObj.transform.position + targetPos.normalized * 8 + new Vector3(0, 0, camZoffset), 7 * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, GameManager.Instance.playerObj.transform.position + new Vector3(0, 0, camZoffset), 10 * Time.deltaTime);
        }

    }
}
