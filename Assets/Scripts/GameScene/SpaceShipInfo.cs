﻿using UnityEngine;
using System.Collections;

public class SpaceShipInfo : MonoBehaviour 
{
    public bool enemy;
    public float maxHp = 100;
    public float hp = 100;

    public int rewardCoin;
    public GameObject hitEffect;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Bullet")
        {
            if (col.GetComponent<bulletMove>().enemy != enemy)
            {
                Hit(col.gameObject);
            }
        }
    }

    void Hit(GameObject obj)
    {
        hp -= obj.GetComponent<bulletMove>().dmg;
        Instantiate(hitEffect, gameObject.transform.position, Quaternion.identity);
        Destroy(obj);

        if(hp<=0)
        {
            //기체가 파괴됨.
            if(enemy)
            {
                GameManager.Instance.KillCount += 1;
                GameManager.Instance.rewardCoin += rewardCoin;
                UImanager_Game.Instance.updateTopPanel();
            }
            else
            {
                GameManager_ROOT.Instance.playing = false;
                GameManager_ROOT.Instance.gameover = true;
                UImanager_Game.Instance.GameOver();
            }

            Destroy(gameObject);
        }
    }
}
