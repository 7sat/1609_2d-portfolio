﻿using UnityEngine;
using System.Collections;

public class PlayerShipCtr : MonoBehaviour 
{
    public Rigidbody2D rb;

    int thrusterCount;
    float spd = 7;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        thrusterCount = transform.GetChild(3).childCount;
        spd = spd * (0.4f * thrusterCount);
    }

	void Update () 
	{
        foreach (Transform t in transform.GetChild(1))
        {
            t.GetComponent<Fire>().shot();
        }
        foreach (Transform t in transform.GetChild(2))
        {
            t.GetComponent<Fire>().shot();
        }
    }

    void FixedUpdate()
    {
        rb.AddForce(transform.up * 1 * spd);

        if(rb.velocity.magnitude > spd)
        {
            rb.velocity = rb.velocity.normalized * spd;
        }

        float h = Input.GetAxis("Horizontal");
        rb.AddTorque(h*-10);
    }
}
