﻿using UnityEngine;
using System.Collections;

public class BorderMove : MonoBehaviour 
{
    public float spd;
    public Vector2 dir;
	
	void Update () 
	{
        transform.Translate(dir * spd * Time.deltaTime);
	}
}
