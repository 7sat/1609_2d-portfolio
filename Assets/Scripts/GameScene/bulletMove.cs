﻿using UnityEngine;
using System.Collections;

public class bulletMove : MonoBehaviour 
{
    public bool enemy;
    public float dmg = 10;

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "MainCamera")
        {
            Destroy(gameObject);
        }
    }
}
