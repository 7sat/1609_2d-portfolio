﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using GameData;
using UnityStandardAssets.ImageEffects;

public class UImanager_Game : MonoBehaviour 
{
    static UImanager_Game instance;
    public static UImanager_Game Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }

    SpaceShipInfo shipInfo;
    float timePassed;

    void Start()
    {
        shipInfo = GameObject.FindWithTag("PlayerShip").GetComponent<SpaceShipInfo>();
        InvokeRepeating("updateHPui", 0.5f, 0.5f);
    }

    public GameObject topPanel;
    public Text timeLeftText;
    public Text KillCountText;
    public Text coinText;

    public void updateTopPanel()
    {
        timePassed = Time.time - GameManager.Instance.startTime;
        timeLeftText.text = Mathf.FloorToInt(timePassed / 60).ToString("00") + ":" + (timePassed % 60).ToString("00.00");

        KillCountText.text = GameManager.Instance.KillCount + "Kill";
        coinText.text = GameManager.Instance.rewardCoin + "Coin";
    }

    public GameObject hpPanel;
    public GameObject hpbar;
    public Text hpText;

    public void updateHPui()
    {
        if (shipInfo == null) return;

        hpText.text = shipInfo.hp + "/" + shipInfo.maxHp;
        hpbar.GetComponent<RectTransform>().sizeDelta = new Vector2((690/shipInfo.maxHp) * shipInfo.hp, 20);
    }

    public GameObject endPanel;
    public Text endkill;
    public Text endtime;
    public Text endcoin;
    public void GameOver()
    {
        GameManager_ROOT.Instance.playGameover();
        StartCoroutine(gameoverCamEffect());
        topPanel.SetActive(false);
        hpPanel.SetActive(false);
        endPanel.SetActive(true);
        endkill.text = GameManager.Instance.KillCount.ToString();
        endtime.text = Mathf.FloorToInt(timePassed / 60).ToString("00") + ":" + (timePassed % 60).ToString("00.00");
        endcoin.text = GameManager.Instance.rewardCoin.ToString();
        GameManager_ROOT.Instance.money += GameManager.Instance.rewardCoin;

        if(GameManager_ROOT.Instance.offlineMode)
        {
            PlayerPrefs.SetInt("offmoney", GameManager_ROOT.Instance.money);
            print("오프라인에 돈 저장" + GameManager_ROOT.Instance.money);
        }
        else
        {
            PlayerPrefs.SetInt("onmoney", GameManager_ROOT.Instance.money);
            StartCoroutine(SaveMoneyCor());
            print("온라인에 돈 저장" + GameManager_ROOT.Instance.money);
        }
        PlayerPrefs.Save();
    }

    IEnumerator gameoverCamEffect()
    {
        Camera.main.transform.GetChild(0).gameObject.SetActive(true);
        float i = 1;
        GameObject effChild = Camera.main.transform.GetChild(0).gameObject;
        while (i>0)
        {
            i -= 0.05f;
            effChild.GetComponent<Grayscale>().rampOffset = i;
            yield return new WaitForSeconds(0.02f);
        }
    }

    public void GotoBuildScene()
    {
        GameManager_ROOT.Instance.playClick();
        GameManager_ROOT.Instance.playLobbyBgm();
        SceneManager.LoadScene("Build");
    }

    IEnumerator SaveMoneyCor()
    {
        string url = "http://52.78.137.62/SpaceShip/SaveMoney.php";
        WWWForm form = new WWWForm();
        form.AddField("userID", PlayerPrefs.GetString("userID"));
        form.AddField("money", GameManager_ROOT.Instance.money);

        WWW www = new WWW(url, form);
        yield return www;

        if (www.error == null)
        {
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text.Trim()) as Dictionary<string, object>;
            string code = responseData["code"].ToString().Trim();

            if (code == "UPDATE_FAIL")
            {
                print(code);
                yield break;
            }
            print(code);
        }
        else
        {
            print("서버연결실패");
        }
    }
}
