﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
    static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    public float startTime;
    public int KillCount;
    public int rewardCoin;

    void Awake()
    {
        instance = this;
    }

    public GameObject playerObj;
    public GameObject[] enemyShipPF = new GameObject[5];

    void Start () 
	{
        playerObj = GameObject.Find("Tank(root)");
        playerObj.GetComponent<Rigidbody2D>().isKinematic = false;

        playerObj.GetComponent<SpaceShipInfo>().maxHp = InfoPanel.Instance.bodyCount * 70;
        playerObj.GetComponent<SpaceShipInfo>().hp = playerObj.GetComponent<SpaceShipInfo>().maxHp;

        StartCoroutine(GenEnemyCor());
        StartCoroutine(StartTimer());
    }

    IEnumerator StartTimer()
    {
        startTime = Time.time;
        while(true)
        {
            UImanager_Game.Instance.updateTopPanel();
            yield return null;
        }
    }

    IEnumerator GenEnemyCor()
    {
        int num = 8;
        while(true)
        {
            for (int i = 0; i < num; i++)
            {
                int type = Random.Range(0, 5);
                GameObject go = Instantiate(enemyShipPF[type], new Vector2(Random.Range(-100, 100), Random.Range(-80, 80)), Quaternion.Euler(0, 0, Random.Range(-90, 90))) as GameObject;

                bool isOverlap = false;
                if (Physics2D.OverlapCircle(go.transform.position, 8, 11))
                {
                    isOverlap = true;
                    while (isOverlap)
                    {
                        Vector2 newPos = new Vector2(Random.Range(-100, 100), Random.Range(-80, 80));

                        if (!Physics2D.OverlapCircle(newPos, 8, 11))
                        {
                            isOverlap = false;
                            go.transform.position = newPos;
                        }
                    }
                }
            }
            num += 2;
            yield return new WaitForSeconds(30f);
        }
    }
}
