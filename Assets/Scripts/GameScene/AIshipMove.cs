﻿using UnityEngine;
using System.Collections;

public class AIshipMove : MonoBehaviour
{
    GameObject TargetObj;

    public float speed;
    public float distLimit;

    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        TargetObj = GameObject.FindWithTag("PlayerShip");
        StartCoroutine(fireCor());
    }

    void FixedUpdate()
    {
        if (TargetObj == null)
        {
            return;
        }

        Vector2 diff = TargetObj.transform.position - transform.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        Quaternion targetRot = Quaternion.Euler(0f, 0f, rot_z - 90);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 2 * Time.deltaTime);

        Debug.DrawLine(transform.position, TargetObj.transform.position, Color.red);

        if (Vector2.Distance(transform.position, TargetObj.transform.position) < 30)
        {
            rb.AddForce(transform.up * speed);
            if (rb.velocity.magnitude > speed)
            {
                rb.velocity = rb.velocity.normalized * speed;
            }
        }
        else
        {
            rb.AddForce(transform.up);
            if (rb.velocity.magnitude > 2)
            {
                rb.velocity = rb.velocity.normalized * 2;
            }
        }
    }

    IEnumerator fireCor()
    {
        while(!GameManager_ROOT.Instance.gameover)
        {
            if (Vector2.Distance(transform.position, TargetObj.transform.position) < 30)
            {
                foreach (Transform t in transform.GetChild(0))
                {
                    t.GetComponent<Fire>().shot();
                }
            }
            yield return null;
        }
    }
}
