﻿using UnityEngine;
using System.Collections;

public class GameManager_ROOT : MonoBehaviour 
{
    static GameManager_ROOT instance;
    public static GameManager_ROOT Instance { get { return instance; } }

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public bool offlineMode;
    public bool playing;
    public bool gameover;
    public int money;

    public bool confirm;
    public bool cancel;

    public AudioSource audioSource;
    public AudioClip lobbyClip;
    public AudioClip gameClip;

    public void playLobbyBgm()
    {
        audioSource.clip = lobbyClip;
        audioSource.Play();
    }
    public void playGameBgm()
    {
        audioSource.clip = gameClip;
        audioSource.Play();
    }

    public AudioSource ac_uiAS;
    public AudioClip ac_click;
    public AudioClip ac_err;
    public AudioClip ac_saved;
    public AudioClip ac_cancel;
    public AudioClip ac_install;
    public AudioClip ac_installCanceled;
    public AudioClip ac_page;
    public AudioClip ac_gameover;

    public void playClick()
    {
        ac_uiAS.clip = ac_click;
        ac_uiAS.Play();
    }
    public void playErr()
    {
        ac_uiAS.clip = ac_err;
        ac_uiAS.Play();
    }
    public void playCancel()
    {
        ac_uiAS.clip = ac_cancel;
        ac_uiAS.Play();
    }
    public void playSave()
    {
        ac_uiAS.clip = ac_saved;
        ac_uiAS.Play();
    }
    public void playInstall()
    {
        ac_uiAS.clip = ac_install;
        ac_uiAS.Play();
    }
    public void playInstallCancel()
    {
        ac_uiAS.clip = ac_installCanceled;
        ac_uiAS.Play();
    }
    public void playPage()
    {
        ac_uiAS.clip = ac_page;
        ac_uiAS.Play();
    }
    public void playGameover()
    {
        ac_uiAS.clip = ac_gameover;
        ac_uiAS.Play();
    }
}
