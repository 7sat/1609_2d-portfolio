﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GameData;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoginManager : MonoBehaviour 
{
    public JoinManager joinManager;
    public InputField idInputField;
    public InputField pwInputField;
    public MessagePopupManager messagePopupManager;

    public void GuestBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        GameManager_ROOT.Instance.offlineMode = true;

        if (!PlayerPrefs.HasKey("off" + 1 + "ListCount"))
        {
            print("오프라인용 저장");

            int[] listcount = new int[] {1, 1, 1};
            int[] itemCode = new int[] { 0, 0, 0 };
            float[] PosX = new float[] { 0, 0, 0 };
            float[] PosY = new float[] { 0, 0, 0 };
            float[] RotZ = new float[] { 0, 0, 0 };

            for (int slotNum = 1; slotNum <= 3; slotNum++)
            {
                PlayerPrefs.SetInt("off" + slotNum + "ListCount", listcount[slotNum - 1]);
                PlayerPrefs.SetInt("off" + slotNum + "_ItemCode" + 0, itemCode[slotNum - 1]);
                PlayerPrefs.SetFloat("off" + slotNum + "_ItemPosX" + 0, PosX[slotNum - 1]);
                PlayerPrefs.SetFloat("off" + slotNum + "_ItemPosY" + 0, PosY[slotNum - 1]);
                PlayerPrefs.SetFloat("off" + slotNum + "_ItemRotZ" + 0, RotZ[slotNum - 1]);
            }
        }

        PlayerPrefs.Save();
        SceneManager.LoadScene("Build");
    }

	public void JoinBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        gameObject.SetActive(false);
        joinManager.gameObject.SetActive(true);
    }

    public void LogInBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        if (idInputField.text.Length<=0)
        {
            GameManager_ROOT.Instance.playErr();
            messagePopupManager.ShowMessage("아이디를 입력하세요");
            return;
        }
        if (pwInputField.text.Length <= 0)
        {
            GameManager_ROOT.Instance.playErr();
            messagePopupManager.ShowMessage("비밀번호를 입력하세요");
            return;
        }

        StartCoroutine(LoginNet());
    }

    IEnumerator LoginNet()
    {
        string url = "http://52.78.137.62/SpaceShip/Login.php";
        WWWForm form = new WWWForm();
        form.AddField("userID", idInputField.text);
        form.AddField("userPW", pwInputField.text);

        WWW www = new WWW(url, form);
        yield return www;

        if(www.error == null)
        {
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text.Trim()) as Dictionary<string, object>;

            string code = responseData["code"].ToString().Trim();

            if (code == "WRONG_PW")
            {
                GameManager_ROOT.Instance.playErr();
                messagePopupManager.ShowMessage("비밀번호 틀림");
                yield break;
            }
            if (code == "NO_DATA")
            {
                GameManager_ROOT.Instance.playErr();
                messagePopupManager.ShowMessage("가입정보 없음");
                yield break;
            }

            Dictionary<string, object> userdata = responseData["data"] as Dictionary<string, object>;

            string json_userdata = MiniJSON.jsonEncode(userdata);
            print("[기본정보] " + json_userdata);

            GameManager_ROOT.Instance.offlineMode = false;

            PlayerPrefs.SetString("userID", idInputField.text);
            PlayerPrefs.SetInt("onmoney", int.Parse(userdata["money"].ToString()));
            PlayerPrefs.Save();
            StartCoroutine(LoadShip());
        }
        else
        {
            GameManager_ROOT.Instance.playErr();
            messagePopupManager.ShowMessage("서버 연결 실패");
        }
    }

    IEnumerator LoadShip()
    {
        string url = "http://52.78.137.62/SpaceShip/LoadShip.php";
        WWWForm form = new WWWForm();
        form.AddField("userID", idInputField.text);

        WWW www = new WWW(url, form);
        yield return www;

        if (www.error == null)
        {
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text.Trim()) as Dictionary<string, object>;
            string code = responseData["code"].ToString().Trim();

            Dictionary<string, object> userdata = responseData["data"] as Dictionary<string, object>;

            string json_userdata = MiniJSON.jsonEncode(userdata);
            string slot1 = "";
            string slot2 = "";
            string slot3 = "";

            print(json_userdata);

            if (json_userdata == "null")
            {
                slot1 = "1/0/0/0/0";
                slot2 = "1/0/0/0/0";
                slot3 = "1/0/0/0/0";
            }
            else
            {
                slot1 = MiniJSON.jsonEncode(userdata["slot1"]);
                slot2 = MiniJSON.jsonEncode(userdata["slot2"]);
                slot3 = MiniJSON.jsonEncode(userdata["slot3"]);
                slot1 = slot1.Remove(0, 1);
                slot1 = slot1.Remove(slot1.Length - 1, 1);
                slot2 = slot2.Remove(0, 1);
                slot2 = slot2.Remove(slot2.Length - 1, 1);
                slot3 = slot3.Remove(0, 1);
                slot3 = slot3.Remove(slot3.Length - 1, 1);
            }

            string[] data1 = slot1.Split('/');
            string[] data2 = slot2.Split('/');
            string[] data3 = slot3.Split('/');

            string[] listCount = new string[3];
            listCount[0] = data1[0];
            listCount[1] = data2[0];
            listCount[2] = data3[0];
            string[][] itemCode = new string[3][];
            itemCode[0] = data1[1].Split(',');
            itemCode[1] = data2[1].Split(',');
            itemCode[2] = data3[1].Split(',');
            string[][] PosX = new string[3][];
            PosX[0] = data1[2].Split(',');
            PosX[1] = data2[2].Split(',');
            PosX[2] = data3[2].Split(',');
            string[][] PosY = new string[3][];
            PosY[0] = data1[3].Split(',');
            PosY[1] = data2[3].Split(',');
            PosY[2] = data3[3].Split(',');
            string[][] RotZ = new string[3][];
            RotZ[0] = data1[4].Split(',');
            RotZ[1] = data2[4].Split(',');
            RotZ[2] = data3[4].Split(',');

            for (int slotNum = 1; slotNum <= 3; slotNum++)
            {
                PlayerPrefs.SetInt("on" + slotNum + "ListCount", int.Parse(listCount[slotNum - 1]));
                for (int i = 0; i < int.Parse(listCount[slotNum - 1]); i++)
                {
                    PlayerPrefs.SetInt("on" + slotNum + "_ItemCode" + i, int.Parse(itemCode[slotNum - 1][i]));
                    PlayerPrefs.SetFloat("on" + slotNum + "_ItemPosX" + i, float.Parse(PosX[slotNum - 1][i]));
                    PlayerPrefs.SetFloat("on" + slotNum + "_ItemPosY" + i, float.Parse(PosY[slotNum - 1][i]));
                    PlayerPrefs.SetFloat("on" + slotNum + "_ItemRotZ" + i, float.Parse(RotZ[slotNum - 1][i]));
                }
            }
            PlayerPrefs.Save();

            SceneManager.LoadScene("Build");
        }
        else
        {
            messagePopupManager.ShowMessage("서버 연결 실패");
        }
    }
}
