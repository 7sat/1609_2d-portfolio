﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessagePopupManager : MonoBehaviour 
{
    public Text infoMessageText;

	public void ShowMessage(string msg)
    {
        gameObject.SetActive(true);
        infoMessageText.text = msg;
    }

    public void ConfirmBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        gameObject.SetActive(false);
    }
}
