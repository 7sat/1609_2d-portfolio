﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GameData;
using System.Collections.Generic;

public class JoinManager : MonoBehaviour 
{
    public InputField idInputField;
    public InputField pwInputField;
    public InputField pwReInputField;

    public LoginManager loginManager;
    public MessagePopupManager msgManager;

    public void JoinBtnClick()
    {
        GameManager_ROOT.Instance.playClick();
        if(idInputField.text.Length==0 || pwInputField.text.Trim().Length==0 || pwReInputField.text.Trim().Length==0)
        {
            msgManager.ShowMessage("회원가입 정보가 입력되지 않았습니다.");
            GameManager_ROOT.Instance.playErr();
            return;
        }

        if(pwInputField.text != pwReInputField.text)
        {
            msgManager.ShowMessage("비밀번호가 일치하지 않습니다.");
            GameManager_ROOT.Instance.playErr();
            return;
        }

        StartCoroutine(JoinNet());
    }

    IEnumerator JoinNet()
    {
        string url = "http://52.78.137.62/SpaceShip/Join.php";
        WWWForm form = new WWWForm();
        form.AddField("userID", idInputField.text);
        form.AddField("userPW", pwInputField.text);

        WWW www = new WWW(url, form);
        yield return www;

        if(www.error == null)
        {
            Dictionary<string, object> responseData =
                MiniJSON.jsonDecode(www.text.Trim()) as Dictionary<string, object>;

            string code = responseData["code"].ToString().Trim();

            if(code == "JOIN_FAIL")
            {
                msgManager.ShowMessage("회원가입 실패");
                GameManager_ROOT.Instance.playErr();
                yield break;
            }
            if(code == "EXIST_USER_ID")
            {
                msgManager.ShowMessage("중복 아이디");
                GameManager_ROOT.Instance.playErr();
                yield break;
            }

            gameObject.SetActive(false);
            msgManager.ShowMessage("회원가입 완료");
            GameManager_ROOT.Instance.playSave();
            loginManager.gameObject.SetActive(true);
        }
        else
        {
            msgManager.ShowMessage("서버연결 실패");
            GameManager_ROOT.Instance.playErr();
        }
    }

    public void CancelBtnClick()
    {
        GameManager_ROOT.Instance.playCancel();
        loginManager.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
